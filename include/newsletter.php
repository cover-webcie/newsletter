<?php

class Newsletter
{
	const ENV_READER = 1;
	const ENV_EDITOR = 2;

	public $template;

	public $submission_date;

	public $sidebar = array();

	public $main = array();

	public $log = array();

	public $unchanged;
	
	public function __construct($template)
	{
		$this->template = $template;

		$this->submission_date = new DateTime();

		$this->sidebar = array();

		$this->main = array();

		$this->log = array();

		$this->unchanged = true;
	}

	public function log($message)
	{
		$this->log[] = array(new DateTime(), $message);
	}

	public function render_title()
	{
		return sprintf('Cover newsletter %s',
			$this->submission_date->format('jS \o\f F'));
	}

	public function render_permalink($format = 'html')
	{
		return URL_TO_NEWSLETTER_ARCHIVE
			. '/' . academic_year()
			. '/' . rawurlencode(sprintf('Newsletter %s.%s',
				$this->submission_date->format('Y-m-d'), $format));
	}

	public function style_headers($html)
	{
		return preg_replace(
			'{<h(\d)>(.+?)</h\1>}',
			'<h\1 style="color:#C60C30">\2</h\1>',
			$html);
	}

	public function style_links($html)
	{
		return preg_replace(
			'{<a (.+?)>}',
			'<a style="color:#FFFFFF" $1>',
			$html);
	}

	public function render($env)
	{
		ob_start();
		include $this->template;
		return ob_get_clean();
	}

	public function render_plain()
	{
		$lines = $this->render_title() . "\r\n\r\n";

		foreach (array_merge($this->main, $this->sidebar) as $section) {
			$lines .= wordwrap(strval($section->render_plain()), 70, "\r\n", true);
			$lines .= "\r\n\r\n";
		}

		return $lines;
	}

	public function render_section($section_id, $mode)
	{
		foreach (array_merge($this->main, $this->sidebar) as $section)
		{
			if ($section->id() != $section_id)
				continue;

			if ($_SERVER['REQUEST_METHOD'] == 'POST')
				$section->handle_postback($_POST);

			if ($mode == 'controls')
				return $section->render_controls();
			else
				return $section->render(self::ENV_EDITOR);
		}
	}

	public function create_section($type, $location, $title='') {
		if ($type === 'agenda')
			$section = new Newsletter_Section_Agenda($title);
		elseif ($type === 'committee_changes')
			$section = new Newsletter_Section_CommitteeChanges($title);
		elseif ($type === 'markdown')
			$section = new Newsletter_Section_Markdown($title);
		else
			throw new Exception('Section type not found!');

		if ($location === 'main')
			$this->main[] = $section;
		elseif ($location === 'sidebar')
			$this->sidebar[] = $section;
		else
			throw new Exception('Location not found!');
	}

	private function _delete_section($section_id, &$array)
	{
		$delete_idx = -1;


		foreach ($array as $idx=>$section) {
			if ($section->id() == $section_id) {
				$delete_idx = $idx;
				break;
			}
		}

		if ($delete_idx > -1) {
			unset($array[$delete_idx]);
			return true;
		}

		return false;
	}

	public function delete_section($section_id)
	{
		// Check main
		if ($this->_delete_section($section_id, $this->main))
			return;

		// Check sidebar
		if ($this->_delete_section($section_id, $this->sidebar))
			return;

		throw new Exception('Section not found!');
	}

	private function _move_section($section_id, &$array)
	{
		$move_idx = -1;


		foreach ($array as $idx=>$section) {
			if ($section->id() == $section_id) {
				$move_idx = $idx;
				break;
			}
		}

		if ($move_idx > 0) {
			$tmp = $array[$move_idx - 1];
			$array[$move_idx - 1] = $array[$move_idx];
			$array[$move_idx] = $tmp;
			return true;
		} elseif ($move_idx == 0) {
			throw new Exception('Section is already on top!');
		}

		return false;
	}

	public function move_section($section_id)
	{
		// Check main
		if ($this->_move_section($section_id, $this->main))
			return;

		// Check sidebar
		if ($this->_move_section($section_id, $this->sidebar))
			return;

		throw new Exception('Section not found!');
	}

	public function render_create_section()
	{
		$section_types = [
			['markdown', 'Markdown'],
			['agenda', 'Agenda'],
			['committee_changes', 'Committee Changes'],
		];

		$structure = '<form method="post" action="?session=' . $_GET['session'] . '">%s %s<button type="submit">Create</button></form>';

		$type_field = '<label for="id_type">Section type</label><select name="type" id="id_type">%s</select>';

		$type_options = array_map(function($type) {
			return sprintf('<option value="%s">%s</option>', $type[0], $type[1]);
		}, $section_types);

		$type_field = sprintf($type_field, join('', $type_options));

		$title_field = '<label for="id_title">Section title</label><input id="id_title" type="text" name="title" placeholder="Title">';
		
		
		return sprintf($structure, $type_field, $title_field);
	}
}
