<?php
function http_json_request($url, array $data=null)
{
	$options = array(
		'http' => $data !== null
			? array(
				'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
				'method'  => 'POST',
				'content' => http_build_query($data))
			: array(
				'method'  => 'GET'
			)
	);

	$context  = stream_context_create($options);

	$response = file_get_contents($url, false, $context);

	if (!$response)
		throw new Exception('No response');

	$data = json_decode($response);

	if ($data === null)
		throw new Exception('Response could not be parsed as JSON');

	return $data;
}

class NewsletterSession
{
	static private $session_id;

	static private $session_details;

	static private $user_authorized;

	static public function login_with_cover_session_id($cover_session_id)
	{
		$data = http_json_request(link_api('session_get_member', $cover_session_id));

		if (empty($data->result))
			throw new Exception('Could not retrieve personal details: ' . $data->error);

		static::$session_id = $cover_session_id;
		static::$session_details = $data->result;
		
		return static::_test_member_is_authorized();
	}

	static private function _test_member_is_authorized()
	{
		$data = http_json_request(
			link_api('session_test_committee', static::$session_id) . '&' . http_build_query(['committee' => array('board', 'candy', 'webcie')])
		);

		if (!empty($data->error))
			throw new Exception('Error from Cover site: ' . $data->error);
		elseif ($data->result != true)
			throw new Exception('User not part of the Board or Candidate Board');

		static::$user_authorized = true;

		return true;
	}

	static public function instance()
	{
		static $instance;
		return $instance ? $instance : $instance = new static;
	}

	private function __construct()
	{
		//
	}

	public function get($temp_id)
	{
		if (isset($_SESSION['newsletter_' . $temp_id]))
			return unserialize($_SESSION['newsletter_' . $temp_id]);
		else
			return false;
	}

	public function set($temp_id, $data)
	{
		$_SESSION['newsletter_' . $temp_id] = serialize($data);
	}

	public function currentUser()
	{
		return new NewsletterSessionUser(static::$session_details ?: array());
	}

	public function sessionId()
	{
		return static::$session_id;
	}

	public function loggedIn()
	{
		return static::$user_authorized === true;
	}
}

class NewsletterSessionUser
{
	public function __construct($data)
	{
		foreach ($data as $property => $value)
			$this->$property = $value;
	}

	public function __toString()
	{
		return isset($this->voornaam) ? $this->voornaam : '[unknown]';
	}
}

class MockNewsletterSession extends NewsletterSession
{
	public function loggedIn()
	{
		return true;
	}

	public function sessionId()
	{
		return 42;
	}

	public function currentUser()
	{
		return new NewsletterSessionUser([
			'id' => 112,
			'voornaam' => 'Andy',
			'achternaam' => 'Clark'
		]);
	}
}