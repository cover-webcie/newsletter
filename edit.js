var session_id = document.location.href.match(/session=([a-z0-9]+)/)[1];

Date.prototype.toDateInputValue = function() {
    var local = new Date(this);
    local.setMinutes(this.getMinutes() - this.getTimezoneOffset());
    return local.toJSON().slice(0,10);
};

var feedback = function(response) {
	alert(response);
};

var createSection = function(section) {
	var edit = function(e) {
		e.preventDefault();

		$.get(this.href, function(html) {
			$(section).html(html);

			$(section).find('form').submit(submit);
		});
	};

	var delete_ = function(e) {
		e.preventDefault();

		if (!confirm('Are you sure you want to delete this section?'))
			return;

		$.post(document.location.href, {
			'action': 'delete_section',
			'section': section.id
		}, function(response) {
			if (response === 'success')
				section.remove();
			else
				feedback(response);
		});
	};

	var moveUp = function(e) {
		e.preventDefault();

		$.post(document.location.href, {
			'action': 'move_section',
			'section': section.id
		}, function(response) {
			if (response === 'success')
				$(section).parent().prev().before($(section).parent());
			else
				feedback(response);
		});
	};

	var submit = function(e)
	{
		$.post(this.action, $(this).serialize(), function(html) {
			$(section).html(html);
			addControls();
		});

		e.preventDefault();
	};

	var addControls = function()
	{
		var $section = $(section);

		// Add empty section info
		if (!$.trim($section.html()))
			$section.append('<span class="empty-section"><h3>Empty section</h3><p>This will not show up in the newsletter</p></span>');

		var $controls = $('<div class="controls"></div>');

		// Add move up button
		$controls.append(
			$('<button class="button">Move up</button>')
				.click(moveUp));

		// Add delete button
		$controls.append(
			$('<button class="button">Delete</button>')
				.click(delete_));

		// Add edit buttons
		$controls.append(
			$('<a class="button">Edit</a>')
				.attr('href', 'index.php?session=' + session_id + '&section=' + section.id + '&mode=controls')
				.click(edit));

		$section.prepend($controls);
	};

	addControls();
}

$(function() {
	var addSection = function(location) {
		var	createPopUp = $('<div><p class="loading">Loading…</p></div>');

		createPopUp.dialog({
			title: 'Add section',
			modal: true,
			width: 400,
			height: 500
		});

		$.get(document.location.href + '&form=create_section', function(html) {
			var $html = $(html);

			createPopUp.find('.loading').remove();

			$html.filter('form').append('<input type="hidden" name="action" value="create_section"><input type="hidden" name="location" value="' + location + '">');

			$html.find('button[type=submit]').click(function(evt) {
				evt.preventDefault();

				var data = $(this).closest('form').serialize();

				$.post(document.location.href, data, function(response) {
					if (response === 'success')
						window.location.reload();
					else
						feedback(response);
				});
			});

			$html.appendTo(createPopUp);
		});
	}

	$('.mailing-editable').each(function() {
		createSection(this);
	});

	var sidebarControls = $('<tr><td class="controls"><button class="button">Add section</button></td></tr>');
	sidebarControls.click(function() {
		addSection('sidebar');
	});

	$('.mailing-sidebar table').append(sidebarControls);

	$('.mailing-main table tr:last').each(function() {
		var controls =  $('<tr><td class="controls"><button class="button">Add section</button></td></tr>');
		controls.click(function() {
			addSection('main');
		});
		controls.insertBefore(this);
	});
	// .find('div:last-child').prev()

})

$(function() {
	var reload = function(response) {
		alert(response);
		document.location.reload();
	};

	var follow = function(response) {
		document.location.href = response;
	};

	var save = function(e) {
		e.preventDefault();

		var filename = prompt('filename', $(document.body).data('filename'));

		if (!filename)
			return;

		$(document.body).data('filename', filename);

		$.post(document.location.href, {
			'action': 'save',
			'name': filename
		}, feedback);
	};

	var load = function(e) {
		e.preventDefault();

		var filepicker = $('<div><p class="loading">Loading…</p></div>');

		filepicker.dialog({
			title: 'Load newsletter',
			modal: true,
			width: 400,
			height: 500
		});

		$.getJSON(document.location.href + '&mode=listing', function(response) {
			filepicker.find('.loading').remove();

			var list = $('<ul/>').appendTo(filepicker);

			list.on('click', 'a', function(e) {
				e.preventDefault();
				filepicker.dialog('close');

				$.post(document.location.href, {
					action: 'load',
					name: $(this).text()
				}, reload);
			});

			$.each(response, function() {
				$('<li>')
					.append($('<a href="#">').text(this.name))
					.append($('<span class="timestamp">').text(this.last_modified))
					.appendTo(list);
			});
		});
	};

	var reset = function(e) {
		e.preventDefault();

		if (!confirm('Do you want to reset the newsletter to the default content?'))
			return;

		$.post(document.location.href, {
			'action': 'reset'
		}, reload);
	};

	var openLog = function(e) {
		e.preventDefault();

		var pre = $('<pre>Loading…</pre>').dialog({
			title: 'Log',
			width: 800,
			height: 500
		});

		$.get(document.location.href + '&mode=log', function(response) {
			pre.text(response);
		});
	};

	var submit = function(e) {
		e.preventDefault();

		var content = $('<div/>')
			.attr('title', 'Submit newsletter')
			.html('\
			<form>\
				<div>\
					<label for="date-of-newsletter">Date of newsletter:</label>\
					<input type="date" name="date" id="date-of-newsletter">\
				</div>\
				<div>\
					<label for="email">Email address:</label>\
					<input type="text" name="email" id="email" value="newsletter@svcover.nl">\
				</div>\
			</form>');

		var submit = function() {
			$.post(document.location.href, {
				action: 'submit',
				date: content.find('#date-of-newsletter').val(),
				email: content.find('#email').val()
			}, feedback);

			$(content).dialog('close');
		};

		$(content).find('#date-of-newsletter').val(new Date().toDateInputValue());

		$(content).find('form').submit(function(e) {
			e.preventDefault();
			submit();
		});

		content.dialog({
			modal: true,
			buttons: {
				'Submit': function() {
					submit();
				},
				'Cancel': function() {
					$(this).dialog('close');
				}
			}
		});
	};

	var logOff = function(e) {
		e.preventDefault();

		$.post(document.location.href, {
			action: 'destroy-session'
		}, follow);
	};

	var nav = $('<ul class="mailing-edit-nav">');

	nav.append('<li>Preview</li>');

	nav.append('<ul><li><a href="index.php?session=' + session_id + '&amp;mode=html" target="_blank">HTML</a></li><li><a href="index.php?session=' + session_id + '&amp;mode=text" target="_blank">Text</a></li></ul>');

	nav.append($('<li>').append($('<a href="#">Save</a>').click(save)));

	nav.append($('<li>').append($('<a href="#">Load</a>').click(load)));

	nav.append($('<li>').append($('<a href="#">Reset</a>').click(reset)));

	nav.append($('<li>').append($('<a href="#">Log</a>').click(openLog)));

	nav.append($('<li>').append($('<a href="#">Submit</a>').click(submit)));

	nav.append($('<li>').append($('<span id="notification-area">')));

	var hideNotificationTimeout;

	window.alert = function(text) {
		clearTimeout(hideNotificationTimeout);

		$('#notification-area').text(text);

		hideNotificationTimeout = setTimeout(function() {
			$('#notification-area').text('');
		}, 5000);
	};

	$.get(document.location.href + '&mode=session', function(response) {
		var item = $('<li>');
		item.css('float', 'right');

		item.append($('<span>').text('Ingelogd als ' + response.voornaam));
		item.append($('<a href="#">Log off</a>').click(logOff));

		nav.append(item);
	});

	$(document.body).prepend(nav);
});