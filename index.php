<?php

error_reporting(E_ALL);
ini_set('display_errors', true);

date_default_timezone_set('Europe/Amsterdam');

require_once 'config.php';
require_once 'include/document.php';
require_once 'include/newsletter.php';
require_once 'include/newsletter_section.php';
require_once 'include/newsletter_section_agenda.php';
require_once 'include/newsletter_section_committeechanges.php';
require_once 'include/newsletter_section_markdown.php';
require_once 'include/newsletterarchive.php';
require_once 'markdown.php';
require_once 'include/session.php';
require_once 'include/template.php';

session_start();

function academic_year()
{
	$year = time() < mktime(0, 0, 0, 9, 1, date('Y'))
		? date('Y') - 1
		: date('Y');

	return sprintf('%d-%d', $year, $year + 1);
}

function link_site($rel = '')
{
	return sprintf('%s://%s/%s',
		COVER_SITE_PROTOCOL,
		COVER_SITE_HOSTNAME,
		$rel);
}

function link_api($method, $session_id=null)
{
	$session = NewsletterSession::instance();

	if (!$session_id)
		$session_id = $session->loggedIn() ? $session->sessionId() : null;

	$session_param = $session_id
		? sprintf('&session_id=%s', $session_id)
		: '';

	return link_site(sprintf('api?method=%s%s', $method, $session_param));
}

function default_newsletter($archive = null)
{
	try {
		$newsletter = $archive->load('DEFAULT');
	} catch(Exception $e) {
		$newsletter = null;
	}

	if (empty($newsletter)) {
		$newsletter = new Newsletter('newsletter.phtml');

		$activities = new Newsletter_Section_Agenda('Actvities');
		$activities->footer = "Every week there is a TAD (Thursday Afternoon Drinks) in the Cover room at 16:00."
			. "\n\n"
			. "The [complete agenda](" . link_site('events/') . ") is available in multiple formats.";
		
		$newsletter->sidebar = [$activities];

		$main = new Newsletter_Section_Markdown('No default template found');
		$main->data = "Add a default template by saving a newsletter with the name DEFAULT";

		$newsletter->main = [$main];
	}

	return $newsletter;
}

function archive_newsletter(Newsletter $newsletter)
{
	$path = PATH_TO_NEWSLETTER_ARCHIVE . sprintf('/%s/Newsletter %s.html',
		academic_year(),
		$newsletter->submission_date->format('Y-m-d'));

	if (!is_dir(dirname($path)))
		if (!mkdir(dirname($path)))
			throw new Exception('Could not create archive directory');

	if (!file_put_contents($path, $newsletter->render(Newsletter::ENV_READER)))
		throw new Exception('Could not archive file');

	$newsletter->log('Archived to ' . $path . ' by ' . NewsletterSession::instance()->currentUser());
}

function submit_newsletter(Newsletter $newsletter, $email)
{
	$plain_text = $newsletter->render_plain();

	$html_text = $newsletter->render(Newsletter::ENV_READER);

	if (!preg_match('{<title>(.+?)</title>}', $html_text, $match))
			throw new Exception('Could not extract subject from newsletter');

	$subject = $match[1];

	$mime_boundary = uniqid('np');

	$message = array(
			"--$mime_boundary",
			"Content-Type: text/plain; charset=UTF-8",
			"Content-Transfer-Encoding: quoted-printable",
			"",
			quoted_printable_encode($plain_text),
			"",
			"--$mime_boundary",
			"Content-Type: text/html; charset=UTF-8",
			"Content-Transfer-Encoding: base64",
			"",
			rtrim(chunk_split(base64_encode($html_text))),
			"",
			"--$mime_boundary--");

	$headers = array(
			'From: Bestuur Cover <board@svcover.nl>',
			'Reply-To: Bestuur Cover <board@svcover.nl>',
			'MIME-Version: 1.0',
			'Content-Type: multipart/alternative;boundary=' . $mime_boundary);

	if(!mail($email,
			$subject,
			implode("\r\n", $message),
			implode("\r\n", $headers)))
		throw new Exception('mail() failed');

	$newsletter->log('Submitted to ' . $email . ' by ' . NewsletterSession::instance()->currentUser());
}

$archive = new NewsletterArchive(PATH_TO_NEWSLETTER_STORE);

$javascript = <<< EOF
<script src="//code.jquery.com/jquery-1.9.1.js"></script>
<script src="//code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<script src="edit.js"></script>
EOF;

$stylesheet = <<< EOF
<link rel="stylesheet" href="//code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css">
<link rel="stylesheet" href="edit.css">
EOF;

if (isset($_GET['session']))
	$temp_id = $_GET['session'];
else
	$temp_id = uniqid();

// Resume the Cover session, if there is one
$session = NewsletterSession::instance();

$error = null;

if (isset($_COOKIE['cover_session_id']))
{
	try {
		NewsletterSession::login_with_cover_session_id($_COOKIE['cover_session_id']);
	} catch (Exception $e) {
		$error = $e->getMessage();
	}
}

if (!$session->loggedIn())
{
	$return_url = sprintf('//%s%s', $_SERVER['HTTP_HOST'], $_SERVER['REQUEST_URI']);
	echo render_template('tpl/login.phtml', compact('error', 'return_url'));
	exit;
}

if (!isset($_GET['session']))
{
	header('Location: index.php?session=' . $temp_id);
	exit;
}

if (!($newsletter = $session->get($temp_id)))
	$newsletter = default_newsletter($archive);

if (isset($_POST['action']))
{
	switch ($_POST['action'])
	{
		case 'reset':
			$newsletter = default_newsletter($archive);
			echo 'Newsletter reset to template';
			break;

		case 'save':
			try {
				$archive->save($newsletter, $_POST['name']);
				echo 'Newsletter saved as ' . $_POST['name'];
			} catch(Exception $e) {
				echo 'Could not save newsletter: ' . $e->getMessage();
			}
			break;

		case 'load':
			try {
				$newsletter = $archive->load($_POST['name']);
				echo 'Newsletter loaded';
			}
			catch (Exception $e) {
				echo 'Could not load newsletter: ' . $e->getMessage();
			}
			break;

		case 'submit':
			try {
				$newsletter->submission_date = new DateTime($_POST['date']);
				archive_newsletter($newsletter);
				submit_newsletter($newsletter, $_POST['email']);
				echo 'Newsletter archived and submitted to ' . $_POST['email'];
			} catch (Exception $e) {
				echo 'Could not submit newsletter: ' . $e->getMessage();
			}
			break;

		case 'delete_section':
			try {
				$newsletter->delete_section($_POST['section']);
				echo 'success';
			} catch (Exception $e) {
				echo $e->getMessage();
			}
			break;

		case 'create_section':
			try {
				$newsletter->create_section($_POST['type'], $_POST['location'], $_POST['title']);
				echo 'success';
			} catch (Exception $e) {
				echo $e->getMessage();
			}
			break;

		case 'move_section':
			try {
				$newsletter->move_section($_POST['section']);
				echo 'success';
			} catch (Exception $e) {
				echo $e->getMessage();
			}
			break;

		case 'destroy-session':
			printf('%s://%s/logout?referrer=%s', COVER_SITE_PROTOCOL, COVER_SITE_HOSTNAME, urlencode(sprintf('//%s%s', $_SERVER['HTTP_HOST'], $_SERVER['REQUEST_URI'])));
			break;
	}
}
else if (isset($_GET['section']))
{
	echo $newsletter->render_section($_GET['section'],
		isset($_GET['mode']) ? $_GET['mode'] : 'html');
}
else if (isset($_GET['form']) && $_GET['form'] == 'create_section')
{
	echo $newsletter->render_create_section();
}
else if (isset($_GET['mode']) && $_GET['mode'] == 'session')
{
	header('Content-Type: application/json');
	echo json_encode($session->currentUser());
}
else if (isset($_GET['mode']) && $_GET['mode'] == 'log')
{
	header('Content-Type: text/plain');

	if (!count($newsletter->log))
		echo 'No messages.';
	else
		foreach ($newsletter->log as $message)
			printf("%s: %s\n", $message[0]->format("Y-m-d H:i"), $message[1]);
}
else if (isset($_GET['mode']) && $_GET['mode'] == 'listing')
{
	header('Content-Type: applicaton/json');
	echo json_encode($archive->listing());
}
else if (isset($_GET['mode']) && $_GET['mode'] == 'text')
{
	header('Content-Type: text/plain');
	echo $newsletter->render_plain();
}
else if (isset($_GET['mode']) && $_GET['mode'] == 'html')
{
	header('Content-Type: text/html; charset=UTF-8');
	echo $newsletter->render(Newsletter::ENV_READER);
}
else
{
	$html = $newsletter->render(Newsletter::ENV_EDITOR);

	// Add additional CSS
	$html = str_replace('</head>', $stylesheet . '</head>', $html);

	// Add JavaScript
	$html = str_replace('</body>', $javascript . '</body>', $html);

	header('Content-Type: text/html; charset=UTF-8');
	echo $html;
}

$session->set($temp_id, $newsletter);
